
import java.util.Scanner;
import java.util.Stack;

public class Tower {

public static void main(String[] args) {
	Scanner input = new Scanner(System.in);
	System.out.print("Enter number of disks: ");
	int n = input.nextInt();

	System.out.println("\nThe moves are:");
	moveDisks(n, 'A', 'B', 'C');

	input.close();

	}

public static Stack<Record> stack = new Stack<Record>();

public static void moveDisks(int n, char fromTower, char toTower, char auxTower) {
	stack.push(new Record(false, n, fromTower, toTower, auxTower));
	while (!stack.isEmpty()) {
	Record record = stack.pop();
	n = record.n;
	fromTower = record.fromTower;
	toTower = record.toTower;
	auxTower = record.auxTower;

	if (record.isLast)
	System.out.println("Move disk " + n + " from " + record.fromTower + " to " + record.toTower);
	else {
	if (n == 1)
	System.out.println("Move disk " + n + " from " + record.fromTower + " to " + record.toTower);
	else {
	stack.push(new Record(false, n - 1, auxTower, toTower, fromTower));
	stack.push(new Record(true, n, fromTower, toTower, auxTower));
	stack.push(new Record(false, n - 1, fromTower, auxTower, toTower));
	}
	}
	}

}

public static class Record {
	boolean isLast = false;
	int n;
	char fromTower;
	char toTower;
	char auxTower;

Record(boolean isLast, int n, char fromTower, char toTower, char auxTower) {
	this.isLast = isLast;
	this.n = n;
	this.fromTower = fromTower;
	this.toTower = toTower;
	this.auxTower = auxTower;
	}
	}

}
